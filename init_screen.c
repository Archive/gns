#include <gnome.h>
#include "gns.h"

GnomeApp *app;
int game_open = 0;
int whose_turn = 0;
int computer_color = 0;
int human_color = 0;
int difficulty = 0;
struct gamecell gameboard[10][10];

void init_board();
void new_game();

void about_cb(GtkWidget *widget, void *data)
{
        GtkWidget *about_box;
        const gchar *authors[] = { "Justin Maurer", "Joe Shaw", NULL };

        about_box = gnome_about_new("GNS", "0.0", "(C) 1998 Justin Maurer",
                                    authors, "Based on the board game Stratego",
				    NULL);

        gtk_widget_show(about_box);
        return;
}

void new_game_cb()
{
	if (game_open != 0) {
	// handle having a game open already
	}
	new_game();
}

void quit_cb()
{
}

gint expose_event(GtkWidget *widget, GdkEventExpose *event)
{
	GdkPixmap *pixmap;
	GdkBitmap *mask;

	pixmap = gdk_pixmap_create_from_xpm(widget->window, &mask, NULL, 
					    "./board.xpm"); 
	gdk_draw_pixmap(widget->window, 
                        widget->style->fg_gc[GTK_WIDGET_STATE(widget)],
			pixmap,
			event->area.x, event->area.y,
			event->area.y, event->area.y,
			event->area.width, event->area.height);
	return FALSE; /* kill the callback */
}

void button_pressed(GtkWidget *widget, GdkEventExpose *event)
{
	g_print("the x and y coordinates are %d and %d\n", event->area.x, 
		event->area.y);
}

static GnomeUIInfo help_menu[] = {
	GNOMEUIINFO_ITEM_STOCK("About", NULL, about_cb, GNOME_STOCK_MENU_ABOUT),
	GNOMEUIINFO_END
};

static GnomeUIInfo game_menu[] = {
	GNOMEUIINFO_ITEM_STOCK("New Game", NULL, new_game_cb,
	                       GNOME_STOCK_MENU_NEW),
	GNOMEUIINFO_ITEM_STOCK("Quit", NULL, quit_cb,
			       GNOME_STOCK_MENU_EXIT),
	GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
	GNOMEUIINFO_SUBTREE("Game", &game_menu),
	GNOMEUIINFO_SUBTREE("Help", &help_menu),
	GNOMEUIINFO_END
};

int prepare_gns(void)
{
	GtkWidget *drawing_area;
	app = GNOME_APP(gnome_app_new("gns", "GNS"));
	gnome_app_create_menus(GNOME_APP(app), main_menu);

	drawing_area = gtk_drawing_area_new();
	gtk_signal_connect(GTK_OBJECT(drawing_area), "expose_event",
			   (GtkSignalFunc) expose_event, NULL);
	gtk_signal_connect(GTK_OBJECT(drawing_area), "button_press_event",
			   (GtkSignalFunc) button_pressed, NULL);
	gtk_widget_set_events(drawing_area, 
			      GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK);
	gtk_widget_set_usize(drawing_area, 320, 320);
	
	gnome_app_set_contents(GNOME_APP(app), drawing_area);
        gtk_signal_connect(GTK_OBJECT(app), "delete_event",
                           GTK_SIGNAL_FUNC(quit_cb), NULL);
	gtk_widget_show(drawing_area);
	gtk_widget_show(GTK_WIDGET(app));
	return 0;
}
