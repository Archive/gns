#include "gns.h"
#include <stdlib.h>
#include <time.h>
#include <glib.h>

#if RAND_MAX
#undef RAND_MAX
#endif

#define RAND_MAX 32

extern struct gamecell gameboard[10][10];
extern int whose_turn;
extern int difficulty;
extern int human_color;
extern int computer_color;

int XOR(int a, int b)
{
	if (a && b) return 0;
	else if (a || b) return 1;
	else return 0;
}

int is_valid_move(struct gamecell *src, struct gamecell *dest)
{
	int rowdiff;
	int coldiff;

	if ((src->piece == FLAG) || (src->piece == BOMB)) return 0;
	if ((dest->habitable == 0) || (src->color == dest->color)) return 0;

	rowdiff = abs(src->row - dest->row);
	coldiff = abs(src->column - dest->column);

	/* Attempting to move diagonally */
	if (!XOR(rowdiff, coldiff)) return 0;
	/* Scouts can move as far as they want in either direction */
	if (src->piece == SCOUT) return 1;
	/* Other pieces can only move one space */
	if (rowdiff > 1 || coldiff > 1) return 0;
	/* Welp, it passed everything. */
	return 1;
}

void battle(struct gamecell *src, struct gamecell *dest)
{
	if (src->piece == dest->piece) {
		src->piece = 0;
		src->color = 0;
		dest->piece = 0;
		dest->color = 0;
		return;
	}

	if (src->piece < dest->piece) {
		src->piece = 0;
		src->color = 0;
		dest->piece = src->piece;
		dest->color = src->color;
		return;
	}
	
	if (src->piece > dest->piece) {
		src->piece = 0;
		src->color = 0;
		return;
	}
}

void lose_spy()
{
}

void spy_kill()
{
}

void explode()
{
}

void victory(int color)
{
}

void hide_piece(struct gamecell cell)
{
}

void end_turn(int nextplayer)
{
	int i, j;

	whose_turn = nextplayer;

	for ( i = 0 ; i < 10 ; i++ ) {
		for ( j = 0 ; j < 10 ; j++) {
			if ((gameboard[i][j].color != nextplayer) || 
			    (gameboard[i][j].color != 0)) 
				hide_piece(gameboard[i][j]);
		}
	}
}

void move(struct gamecell *src, struct gamecell *dest)
{
	int nextplayer = dest->color;

	if (dest->piece == FLAG) {
		victory(src->color);
		return;
	}

	if ((dest->piece == BOMB) && (src->piece != MINER)) {
		src->color = 0;
		src->piece = 0;
		dest->color = 0;
		dest->piece = 0;
		end_turn(nextplayer);
		explode(); 
		return;
	}	

	if (dest->piece == SPY) {
		/* whoever did NOT move, loses their spy */
		dest->color = src->color;
		dest->piece = src->piece;
		src->color = 0;
		src->piece = 0;
		end_turn(nextplayer);
		lose_spy();
		return;
	}
	
	if (src->piece == SPY) {
		/* whoever moved wins */
		dest->color = src->color;
		dest->piece = src->piece;
		src->color = 0;
		src->piece = 0;
		end_turn(nextplayer);
		spy_kill();
		return;
	}

	if (dest->piece == 0) {
		dest->piece = src->piece;
		dest->color = src->color;
		src->color = 0;
		src->piece = 0;
		end_turn(nextplayer);
		return;
	}

	else {
		battle(src, dest);
	}
}

int add_piece(int piece)
{
	if (piece == FLAG) return 0;
	
	if (difficulty == 0) {
		if (piece == SPY) return 5;
		if (piece == BOMB) return 3;
		else return piece;
	}

	return 0;
}
int take_scores(int color)
{
	int score, i, j;
	score = 0;

	for (i = 0; i < 10 ; i++ ) {
		for (j = 0 ; j < 10 ; j++) {
			if (gameboard[i][j].color == color) {
				score += add_piece(gameboard[i][j].piece);
			}
		}
	}

	if (difficulty == 0) return (score * 100) / 23600;
	return 0;
}

struct gamecell *find_dest(struct gamecell *src)
{
	int randnum;
	struct gamecell *cell;
	cell = malloc(sizeof(struct gamecell));
	
	srand(time(NULL));
	randnum = rand();

	if (difficulty == 0) {

		if (randnum > 15) {
			if (gameboard[src->row-1][src->column].color == 
			    human_color) {
				cell = &gameboard[src->row-1][src->column];
				return cell; 
			}
			if (gameboard[src->row][src->column+1].color == 
			    human_color) {
				cell = &gameboard[src->row+1][src->column];
				return cell; 
			}
			if (gameboard[src->row+1][src->column].color == 
			    human_color) {
				cell = &gameboard[src->row+1][src->column];
				return cell;
			}
			if (gameboard[src->row][src->column-1].color == 
			    human_color) {
				cell = &gameboard[src->row][src->column-1];
				return cell;	
			}
		}
		else {
			randnum = randnum / 8;
			switch (randnum) {
				case 0: 
				    cell = &gameboard[src->row-1][src->column];
			            return cell; 
				case 1:
				    cell = &gameboard[src->row][src->column+1];
				    return cell; 
				case 2:
				    cell = &gameboard[src->row+1][src->column];	
		                    return cell; 
				case 3:
			            cell = &gameboard[src->row][src->column-1]; 
				    return cell; 
				default:
				     cell = &gameboard[src->row-1][src->column];
				     return cell; 
			}
		}
	}
	return &gameboard[src->row-1][src->column]; 
}

int find_num_pieces(int color)
{
	int i, j, returnme;

	returnme = 0;
	for (i=0;i<10;i++) {
		for (j=0;j<10;j++) {
			if (gameboard[i][j].color == color) returnme++;
		}
	}
	
	return returnme;
}

void ai_move_nice()
{
	struct gamecell *piece, *src;
	int randnum, i, j, counter;
	counter = 0;
	src = malloc(sizeof(struct gamecell));
	piece = malloc(sizeof(struct gamecell));

	srand(time(NULL));
	randnum = rand();	
	
	if (randnum > find_num_pieces(computer_color)) {
 		ai_move_nice();
		return;
	}

	else {
		for (i = 0 ; i < 10 ; i++) {
			for (j = 0 ; j < 10 ; j++) {
				if (gameboard[i][j].color == computer_color) 
					counter++;
				if (counter == randnum) {
					piece = find_dest(&gameboard[i][j]);
					if (piece->color == 0) {
						ai_move_nice();
						return;
					}
					else {
						src = &gameboard[i][j];
						move(src, piece);
						return;
					}
				}
			}
		}
	}
}

void ai_move_good()
{
}

void ai_move()
{
	int human_score, ai_score;
	
	human_score = take_scores(human_color);
	ai_score = take_scores(computer_color);

	if (ai_score >= human_score) ai_move_nice();
	else ai_move_good();

	return;
}

