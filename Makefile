gns: main.o init_screen.o board.o libgns.o
	gcc -o gns -g main.o init_screen.o board.o libgns.o  \
	`gnome-config --libs gnomeui`

main.o: main.c gns.h
	gcc -c -g main.c `gnome-config --cflags gnomeui` -Wall

init_screen.o: init_screen.c gns.h
	gcc -c -g init_screen.c `gnome-config --cflags gnomeui` -Wall

board.o: board.c gns.h
	gcc -c -g board.c `gnome-config --cflags gnomeui` -Wall

libgns.o: libgns.c gns.h
	gcc -c -g libgns.c `gnome-config --cflags gnomeui` -Wall

clean:
	rm gns *.o
