#include <gnome.h>
#include "gns.h"

static const struct poptOption options[] = {
#ifdef HAVE_ORBIT
  {"ior", '\0', POPT_ARG_STRING, &ior, 0, N_("IOR of remote GNS server"),
   N_("IOR")},
#endif
};

int prepare_gns();

int main(int argc, char *argv[])
{
#ifdef HAVE_ORBIT
	CORBA_def(CORBA_Environment ev;)

        CORBA_exception_init (&ev);
        orb = gnome_CORBA_init_with_popt_table("gns", VERSION, &argc, 
					       argv, options, 0, NULL, 
                                               GNORBA_INIT_SERVER_FUNC | 
                                               GNORBA_INIT_DISABLE_COOKIES, 
	 				       &ev);
#else
        gnome_init_with_popt_table("gns", VERSION, argc, argv, options, 0, 
				   NULL);
#endif /* HAVE_ORBIT */

	prepare_gns();
	gtk_main();
	return 0;
}
