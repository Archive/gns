#ifndef _STRATEGO_H
#define _STRATEGO_H

#define VERSION "0.0"

struct gamecell {
	int color;
	int piece;
	int habitable; /* thanks joe */
	int row;
	int column;
};
 
enum { EMPTY, MARSHAL, GENERAL, COLONEL, MAJOR, CAPTAIN, LIEUTENANT, SERGEANT, 
       MINER, SCOUT, SPY, BOMB, FLAG };

#endif /* _STRATEGO_H */
